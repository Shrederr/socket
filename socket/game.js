import {texts} from "../data";
import {SECONDS_TIMER_BEFORE_START_GAME} from "./config";
import {SECONDS_FOR_GAME} from "./config";
import {sortArray} from "../public/helper/domHelper";
import {getRandomInt} from "../public/helper/domHelper";




let gameStatus = "none";
let usersNameCollection = [];
let usersCollection = [];
let speedOfUsers = [];
let winnerList = [];
let resultOfEndingGame = 0;
let resultArray = [];
let textNumber = getRandomInt(texts.length);
export default io => {
    io.on("connection", socket => {
        const username = {name: socket.handshake.query.username, id: socket.id, status: false}
        if (usersNameCollection.includes(username.name) === false && username.name !== null) {
            usersCollection.push(username);
            usersNameCollection.push(username.name);
            socket.emit('ADD_LIST_USERS', usersCollection);
            socket.broadcast.emit('UPDATE_USERS_LIST', username, false);
        } else {
            socket.emit("USER_WITH_SUCH_NAME_IS_EXIST");
        }

        socket.on('CHANGE-STATUS', (userWhoChangeStatus, status) => {
            let statusChecked = 0;
            for (let i = 0; i < usersCollection.length; i++) {
                if (usersCollection[i].name.toString() === userWhoChangeStatus) {
                    usersCollection[i].status = status;
                    socket.broadcast.emit('SEND_NEW_STATUS_OF_USER', usersCollection[i]);
                }
                if ((usersCollection[i].status) === "ready") {
                    statusChecked++;
                }
            }
            if (statusChecked === usersCollection.length) {
                socket.emit('ALL_USERS_IS_READY', SECONDS_TIMER_BEFORE_START_GAME, textNumber);
                socket.broadcast.emit('ALL_USERS_IS_READY', SECONDS_TIMER_BEFORE_START_GAME, textNumber);
            }
        })
        socket.on("CHECK-STATUS_WHEN_SOMEBODY_LEAVE", () => {
            if (gameStatus === "none") {
                let statusChecked = 0;
                for (let i = 0; i < usersCollection.length; i++) {
                    if ((usersCollection[i].status) === "ready") {
                        statusChecked++;
                    }
                }
                if (statusChecked === usersCollection.length) {
                    socket.emit('ALL_USERS_IS_READY', SECONDS_TIMER_BEFORE_START_GAME, 0);
                }
            }
        })
        socket.on("READY_TO_START_GAME", () => {
            gameStatus = "playing";
            socket.emit("START_GAME", SECONDS_FOR_GAME);
        })
        socket.on("UPDATE_STATUS_BAR", (progress, username, userEndGame) => {
            if (userEndGame) {
                speedOfUsers.push(username);
                if (speedOfUsers.length === usersCollection.length) {
                    socket.emit("ALL_USERS_FINISHED");
                    socket.broadcast.emit("ALL_USERS_FINISHED");
                }
            }

            socket.broadcast.emit("SEND_STATUS_BAR", progress, username);
        })
        socket.on("STATUS_BAR_TO_ZERO",(zero,username)=>{
            socket.emit("SEND_STATUS_BAR",zero, username)
            socket.broadcast.emit("SEND_STATUS_BAR", zero, username);
        });
        socket.on('END_GAME_RESULT', result => {
            resultOfEndingGame++;
            gameStatus = "none";
            resultArray.push(result);
            if (resultOfEndingGame === usersNameCollection.length) {
                winnerList = [];
                sortArray(resultArray);
                if (speedOfUsers.length !== 0) {
                    speedOfUsers.forEach((item) => {
                        winnerList.push(item);
                    });
                }
                resultArray.forEach((item) => {
                    if (!winnerList.includes(item.username)) {
                        winnerList.push(item.username);
                    }
                })
                socket.emit("SEND_RESULT_LIST", winnerList);
                socket.broadcast.emit("SEND_RESULT_LIST", winnerList);
                speedOfUsers = [];
                winnerList = [];
                resultOfEndingGame = 0;
                resultArray = [];
                textNumber =getRandomInt(texts.length);
                socket.broadcast.emit("UPDATE_USERS_LIST");

            }
        })
        socket.on("disconnect", () => {
            let checkOfFakeUser = usersCollection.length;
            if (usersCollection.length > 1) {
                for (let i = 0; i < usersCollection.length; i++) {
                    if ((usersCollection[i].id === socket.id)) {
                        if (speedOfUsers.includes(usersCollection[i].name)) {
                            speedOfUsers.forEach((item)=>{
                                if(item === usersCollection[i].name){
                                    speedOfUsers.splice(item,1);
                                }
                            })
                        }
                        usersCollection.splice(i, 1);
                        usersNameCollection.splice(i, 1);

                    }
                }

            } else if (usersCollection[0] && usersCollection[0].id === socket.id) {
                usersCollection = [];
                usersNameCollection = [];
            } else {
                usersCollection = [];
                usersNameCollection = [];
            }
            if (checkOfFakeUser !== usersNameCollection.length) {
                socket.broadcast.emit("UPDATE_USERS_LIST", socket.handshake.query.username, true);
            }
            console.log(`${socket.id} disconnected`);
        });
    });

};
