import { Router } from "express";
import path from "path";
import { HTML_FILES_PATH } from "../config";
import  {texts} from "../data";

const router = Router();

router
  .get("/", (req, res) => {
    const page = path.join(HTML_FILES_PATH, "game.html");
    res.sendFile(page);
  })
    .get("/texts/:id", (req, res) => {
       try{
        res.data = texts[req.params.id];
           res.status(200).send(
               res.data
           );
           }catch (error){
           res.status(500).send({
               error: true,
               message: error.message
           });
       }
     })

export default router;
