import {showModal} from './modal.js';
import {createElement} from "../helper/domHelper.js";

export function showWinnerModal(winnerList) {
    let divWinners = createElement({
        tagName: 'div',
    });
    winnerList.forEach((item, i) => {
        let candidat = (createElement({
            tagName: "div",
            className: "userResult",
            attributes: {
                style: `innerText:#${i + "" + item}`
            }
        }))
        candidat.innerText = `#${(i + 1) + " " + item}`
        divWinners.append(candidat);
    })

    function onClose() {
        divWinners.remove();
        const readyButton = document.getElementById('ready-button');
        readyButton.classList.remove('display-none');
    }

    showModal({title: 'winner List', bodyElement: divWinners, onClose});
    // call showModal function
}
