import {createElement} from "../helper/domHelper.js";
import {fight} from "./speedFighting.mjs";
import {showWinnerModal} from "../modal/winner.js";


let users = [];
let text = "";
const usersList = document.getElementById("users-lists");
const username = sessionStorage.getItem("username");
const readyButton = document.getElementById('ready-button');
const noReadyButton = document.getElementById('no-ready-button');
const status = "false";
const timerForStart = document.getElementById("timerForStart");
if (!username) {
    window.location.replace("/login");
}
const socket = io("http://localhost:3002/game", {query: {username, status}});

const addListOfUsers = usersArray => {
    for (let i = 0; i < usersArray.length; i++) {

        if (usersArray[i].name !== undefined) {

            addNewUserToList(usersArray[i]);
        }
    }
}
const updateListOfUsers = (username, leave) => {
    if (leave) {
        document.getElementById(username + "").remove();
        socket.emit("CHECK-STATUS_WHEN_SOMEBODY_LEAVE");
    } else if (users.includes(username.name) === false) {
        addNewUserToList(username);
    }
}

const waitingOfStart = (time, textId) => {
    let timeForStart = time;
    noReadyButton.classList.add('display-none');
    timerForStart.classList.remove('display-none');
    timerForStart.innerText = timeForStart.toString();
    fetch(`http://localhost:3002/game/texts/${textId}`, {method: "GET",}).then((res) => {
        res.text().then((toText) => {
            text = toText;
        })
        let timer = setInterval(() => {
            if (timeForStart <= 0) {
                timerForStart.classList.add('display-none');
                socket.emit("READY_TO_START_GAME");
                clearInterval(timer);
            } else {
                timeForStart--;
                timerForStart.innerText = timeForStart.toString();
            }
        }, 1000);
    });
}
const ready = () => {
    document.querySelector(`.${username + "circle"}`).style.backgroundColor = "green";
    sessionStorage.setItem("status", "ready");
    readyButton.classList.add('display-none');
    noReadyButton.classList.remove('display-none');
    socket.emit("CHANGE-STATUS", username, "ready");
}
const noReady = () => {
    document.querySelector(`.${username + "circle"}`).style.backgroundColor = "red";
    sessionStorage.setItem("status", "noReady");
    noReadyButton.classList.add('display-none');
    readyButton.classList.remove('display-none')
    socket.emit("CHANGE-STATUS", username, "noReady");
}
const addNewUserToList = (name) => {
    const newUsers = createElement({
        tagName: "div",
        className: "user",
        attributes: {
            id: `${name.name}`
        }
    })
    const statusCircle = createElement({
        tagName: "div",
        className: `statusCircle ${name.name + "circle"} `,
    })
    const progressBarWrapper = createElement({
        tagName: "div",
        className: `status-bar-wrapper `
    })
    const progressBar = createElement({
        tagName: "div",
        className: `status-bar ${name.name}`,
        attributes:{
            style: "width:0%;"
        }
    })

    newUsers.innerText = name.name;
    if (name.name === username) {
        newUsers.innerText = name.name + "(you)";
        if (name.status === "ready") {
            readyButton.classList.add('display-none');
            noReadyButton.classList.remove('display-none');
        } else {
            noReadyButton.classList.add('display-none');
            readyButton.classList.remove('display-none');
        }
    }
    if (name.status === "ready") {
        statusCircle.style.backgroundColor = "green";

    } else {
        statusCircle.style.backgroundColor = "red";
    }
    users.push(name.name);
    usersList.append(newUsers);
    newUsers.append(statusCircle);
    progressBarWrapper.append(progressBar);
    newUsers.append(progressBarWrapper);
}
readyButton.addEventListener("click", ready);
noReadyButton.addEventListener("click", noReady)

socket.on("SEND_NEW_STATUS_OF_USER", (username) => {
    if (username.status === "ready") {
        document.querySelector(`.${username.name + "circle"}`).style.backgroundColor = "green";
    } else {
        document.querySelector(`.${username.name + "circle"}`).style.backgroundColor = "red";
    }
})

socket.on("START_GAME", (time) => {
        const fighting = fight(text, time, username);
        fighting.then((result) => {
            socket.emit("END_GAME_RESULT", result);
        });
});
socket.on("UPDATE_USERS_LIST", updateListOfUsers);
socket.on("SEND_STATUS_BAR", (progress, userWhoUpdateStatus) => {
    const progressBar = document.querySelector(`.${userWhoUpdateStatus}`);
    progressBar.style.width = progress + "%";
    if (progress === 100) {
        progressBar.style.backgroundColor = "green";
    }
});
socket.on("ADD_LIST_USERS", addListOfUsers);
socket.on('ALL_USERS_IS_READY', waitingOfStart);
socket.on("SEND_RESULT_LIST", (winnerList) => {
    document.querySelector(`.${username + "circle"}`).style.backgroundColor = "red";
    socket.emit("CHANGE-STATUS", username, "noReady");
    socket.emit("");
    showWinnerModal(winnerList);
});
socket.on("USER_WITH_SUCH_NAME_IS_EXIST", () => {
        alert("User with name is exist!");
        sessionStorage.removeItem("username");
        window.location.replace("/login");

    }
)

