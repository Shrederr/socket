import {createElement} from "../helper/domHelper.js";

export async function fight(text, time, username) {
    return new Promise((resolve) => {
        const socket = io("http://localhost:3002/game", {query: {username}});
        const gameFill = createElement({
            tagName: 'span',
            attributes: {
                id: "game-fill"
            }
        })
        const progressBar = document.querySelector(`.${username}`);
        progressBar.style.width = "0%";
        let progress = 0;
        let actualChar = 0;
        let textLength = text.length;
        let arrayChar = Array.from(text);
        document.getElementById('start-game').append(gameFill);
        document.getElementById('start-game').classList.remove("display-none");
        arrayChar.forEach((item, i) => {
            const char = createElement({
                tagName: "pre"
            });
            char.innerText = item;
            arrayChar[i] = char;
            gameFill.append(arrayChar[i]);
        })
        let timeForStart = time;
        document.getElementById('gameTimer').classList.remove('display-none');
        document.getElementById('gameTimer').innerText = timeForStart.toString();
        let timer = setInterval(() => {
            if (timeForStart <= 0) {
                document.getElementById('gameTimer').classList.add('display-none');
                document.getElementById('game-fill').remove();
                document.removeEventListener('keydown', handle);
                resolve({actualChar, username});
                clearInterval(timer);
            } else {
                timeForStart--;
                document.getElementById('gameTimer').innerText = timeForStart.toString();
            }
        }, 1000);

        function handle(event) {

            if ((event.key).charCodeAt(0) === text[actualChar].charCodeAt(0)) {

                arrayChar[actualChar].style.backgroundColor = 'rgba(100, 100, 100, 0.5)';
                arrayChar[actualChar].style.borderBottom = "none";
                if ((actualChar + 1) < textLength) {
                    arrayChar[actualChar + 1].style.borderBottom = "0.5px solid black";
                }
                progress += 100 / textLength;
                progressBar.style.width = progress + "%";
                socket.emit("UPDATE_STATUS_BAR", progress, username, false);
                actualChar++;
                if (actualChar === textLength) {
                    progressBar.style.backgroundColor = "green";
                    progress = 100;
                    socket.emit("UPDATE_STATUS_BAR", progress, username, true);
                    progress = 0;
                    document.removeEventListener('keydown', handle);
                }
            }

        }


        document.addEventListener('keydown', handle);
        socket.on("ALL_USERS_FINISHED", () => {
            document.getElementById('gameTimer').classList.add('display-none');
            document.getElementById('game-fill').remove();
            socket.emit("STATUS_BAR_TO_ZERO",0,username);
            document.removeEventListener('keydown', handle);
            resolve({actualChar, username});
            clearInterval(timer);
        })
    });
    // resolve the promise with the winner when fight is over
}
