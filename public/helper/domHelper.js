export function createElement({tagName, className, attributes = {}}) {
    const element = document.createElement(tagName);

    if (className) {
        const classNames = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));
    return element;
}

export function sortArray(arr) {
    if(arr.length>2){
    arr.sort((a, b) => a.actualChar < b.actualChar ? 1 : -1);
}
}
export function getRandomInt(max){
    return Math.floor(Math.random() * Math.floor(max));
}
